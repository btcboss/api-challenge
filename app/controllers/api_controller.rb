class ApiController < ApplicationController

	require 'open-uri'
	require 'nokogiri'

	
	def create

		page_string = ""
		url = params[:url]

		begin 

			doc = Nokogiri::HTML(open(url))
			page_string = doc.css("h1, h2, h3, a").map(&:text).to_s

		rescue => error
			render json: { status: 400, errors: error.message} and return
		end

		website_content = WebsiteContent.create(url: url, content: page_string)


		if website_content.valid?
			msg = { status: 200 }
		else
			msg = { status: 400, errors: website_content.errors}
		end

		render json: msg
	end

	def index

		website_content = WebsiteContent.select("url, content").all
	
		render json: website_content

	end

end