Rails.application.routes.draw do

    # To save website content
    post 'api' => 'api#create'
    # To retrieve website content
    get 'api' => 'api#index'


end
