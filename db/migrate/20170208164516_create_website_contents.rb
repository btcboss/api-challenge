class CreateWebsiteContents < ActiveRecord::Migration
  def change
    create_table :website_contents do |t|
      t.string :url
      t.string :content

      t.timestamps null: false
    end
  end
end
